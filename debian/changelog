ibus-array (0.2.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release. 
  * debian/rules: Drop workaround on python version, merged upstream.
  * debian/control: Bump Standards-Version to 4.5.1.
  * debian/patches/*: Drop old patches, all merged upstream.
  * debian/patches/0001-setup.py: Add patch to hardcode shebang
    to be /usr/bin/python3.

 -- Boyuan Yang <byang@debian.org>  Sat, 26 Dec 2020 13:05:50 -0500

ibus-array (0.2.1-6) unstable; urgency=medium

  * Team upload.

  [ Changwoo Ryu ]
  * debian/rules:
    + Do not use --libexecdir configure flag anymore. (Closes: #955226)
    + Enable hardening=+all build option
  * debian/patches/specify-setup-in-ibus-component.patch: Specify the
    setup path in the IBus component XML
  * debian/control:
    + Bump Standards-Version to 4.5.0
    + Use debhelper-compat (= 12) instead of debhelper dependency
    + Drop useless B-D on dh-autoreconf
    + Add "Rules-Requires-Root: no"
  * debian/compat: Dropped
  * debian/copyright: Update to the copyright-format 1.0 format
  * Correct patch tags

  [ Anthony Fok ]
  * debian/patches/fix-preedit-text-behaviour-at-engine-reset.patch:
    Hide instead of update the preedit text upon engine reset.
    + This fixes a disappearing cursor issue seen on VTE-based terminals,
      and removes a left-behind lookup table fragment seen on XTerm.
    + This also fixes a problem where text would get deleted upon
      double-click selection in applications such as Chromium.
  * debian/patches/update-version-number-and-array-xml.patch:
    + Update version number in configure.ac
    + add <symbol>&#x884C;</symbol> to array.xml so that the Array input
      method shows up as "行" rather than something like "zh₁".
  * debian/gbp.conf: Add "pristine-tar = True"

 -- Anthony Fok <foka@debian.org>  Fri, 05 Jun 2020 04:38:16 -0600

ibus-array (0.2.1-5) unstable; urgency=medium

  * Team upload.
  * debian/control:
    + Bump Standards-Version to 4.4.1.
    + Add back Vcs-* information. The packaging git repository is
      currently under Salsa input-method-team namespace.

 -- Boyuan Yang <byang@debian.org>  Sun, 19 Jan 2020 15:07:22 -0500

ibus-array (0.2.1-4) unstable; urgency=medium

  * Team upload
  * Use python3 (Closes: #936717)
    - debian/patches/python3: Upstream commit to fix syntax

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 08 Dec 2019 00:49:25 +0900

ibus-array (0.2.1-2) unstable; urgency=low

  * Changed Maintainer to Debian Input Method Team
    <debian-input-method@lists.debian.org> (Closes: #903714)
  * Bump Standards-Version to 4.2.1
  * Temporarily remove VCS-*, I will need some time to clean the
    code before transition to salsa.

 -- Keng-Yu Lin <kengyu@lexical.tw>  Fri, 12 Oct 2018 04:54:44 +0800

ibus-array (0.2.1-1) unstable; urgency=low

  * New upstream release
  * Remove pkg-ime-devel@lists.alioth.debian.org (Closes: 899956)

 -- Keng-Yu Lin <kengyu@lexical.tw>  Tue, 05 Jun 2018 18:24:24 +0800

ibus-array (0.1.2-1) unstable; urgency=low

  * New upstream release

 -- Keng-Yu Lin <kengyu@lexical.tw>  Thu, 26 Jan 2017 15:22:22 +0800

ibus-array (0.1.1-1) unstable; urgency=low

  * Imported Upstream version 0.1.1
  * Fix debian/watch
  * Update new upstream URL

 -- Keng-Yu Lin <kengyu@lexical.tw>  Fri, 28 Oct 2016 16:02:27 +0800

ibus-array (0.1.0-1) unstable; urgency=low

  * Imported Upstream version 0.1.0

 -- Keng-Yu Lin <kengyu@lexical.tw>  Sat, 01 Nov 2014 20:48:51 +0800

ibus-array (0.0.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Update watch file (good enough for uscan --report).
  * Update package dependency to python-gi.  Closes: #766467

 -- Osamu Aoki <osamu@debian.org>  Thu, 23 Oct 2014 23:19:59 +0900

ibus-array (0.0.3-1) unstable; urgency=medium

  * New upstream release

 -- Keng-Yu Lin <kengyu@lexical.tw>  Thu, 16 Oct 2014 18:11:05 +0800

ibus-array (0.0.2-10) unstable; urgency=low

  * Use Goject introspection and drop the dependency on
    ibus-python (Closes: #746968)

 -- Keng-Yu Lin <kengyu@lexical.tw>  Mon, 19 May 2014 22:08:22 +0800

ibus-array (0.0.2-9) unstable; urgency=low

  [ Nicolas Sévelin-Radiguet ]

  * fix FTBFS with clang instead of gcc (Closes: #743130)

 -- Keng-Yu Lin <kengyu@lexical.tw>  Tue, 22 Apr 2014 17:18:02 +0800

ibus-array (0.0.2-8) unstable; urgency=low

  * Package with policy=3.9.4, dh_python2 and compat=9.
  * Dependency to python-ibus added for ibus 1.5.

 -- Osamu Aoki <osamu@debian.org>  Sat, 07 Sep 2013 02:24:45 +0900

ibus-array (0.0.2-7) unstable; urgency=low

  * Fix libexecdir. Closes: #715180

 -- Osamu Aoki <osamu@debian.org>  Sun, 07 Jul 2013 16:13:17 +0900

ibus-array (0.0.2-6) unstable; urgency=low

  [ Colin Watson ]

  * Build-depend on libibus-1.0-dev rather than libibus-dev. (Closes:bug#640709)
  * Follow ibus 1.4 config API change.

  [ Keng-Yu Lin ]

  * debian/patches/clear_error_type.patch
    - Fixes the issue of error code showing in some circumstances.

  * debian/patches/remove_embedded_sqlite.patch
    - removed the SQLite lib embedded in the package.

 -- Keng-Yu Lin <kengyu@lexical.tw>  Tue, 13 Sep 2011 12:41:16 +0800

ibus-array (0.0.2-5) unstable; urgency=low

  [ Bhavani Shankar ]

  * link against -ldl to fix FTBFS (Closes: bug#615732)

 -- Keng-Yu Lin <kengyu@lexical.tw>  Wed, 23 Mar 2011 21:08:01 +0800

ibus-array (0.0.2-4) unstable; urgency=low

  * removed build-depends on quilt and README.source

 -- Keng-Yu Lin <kengyu@lexical.tw>  Wed, 27 Oct 2010 00:14:41 +0800

ibus-array (0.0.2-3) unstable; urgency=critical

  * Switch to dpkg-source 3.0 (quilt) format
  * fixed crash of gobject_unref() assertion on libibus2 (Closes: bug#596975)

 -- Keng-Yu Lin <kengyu@lexical.tw>  Thu, 16 Sep 2010 13:37:29 +0800

ibus-array (0.0.2-2) unstable; urgency=low

  * Add autopoint, remove cvs from Build-depends (Closes: bug#572472)

 -- Keng-Yu Lin <kengyu@lexical.tw>  Mon, 08 Mar 2010 17:09:40 +0800

ibus-array (0.0.2-1) unstable; urgency=low

  * Initial release (Closes: bug#559421)
  * add DH7 support by Paul Liu <paulliu@debian.org>

 -- Keng-Yu Lin <kengyu@lexical.tw>  Fri, 18 Dec 2009 17:53:30 +0800
